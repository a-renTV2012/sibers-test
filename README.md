# Sibers test
Test task for Sibers.

## How to install
You need:
- Any webserver (Apach or nginx are fine).
- PHP 7.0 or newer.
- MySQL 5.0 or newer (probably other DBMS will understand the queries too).

All this can be installed with "sudo apt-get install lamp" on most Linux systems.  
Once you finished the setup, put all files into your webserver sites'/<your site> directory. Then, give your DBMS an "db_dump.sql" file and put your values in "php/settings.php". You are good to go!

Admin's login/password for web interface: Andreyno/Secret.

## How it works
#### /
index.php is the page you get when connecting to a website. There is some logic (so it can decide to show login screen or the interface), but mostly it's just an HTML markup.  
db_dump.sql is the dump of the test database.

#### /php
There you can find the main classes - usersManager and authenticator. First is responsible for interacting with users table in database and giving you necessary data in a safe way. The second takes care of authentication, which consists of session and page identifiers validation. So you can only collect data from API if you're authorized and use web interface.  
This folder also contains db connection settings file.
##### /php/api
That's the folder where our request handlers are stored. Any data you send to the server is received and validated here. In different files the different actions are performed.

#### /js
This directory contains vital scripts for the interface. Login.js makes request to the authentication handler, two classes in index.js control render ("presenter") and updating the database ("updater").

#### /css
Just some styles for the webpage so it doesn't look VERY bad.

#### /static
Images and such stuff that doesn't change over development process (unless replaced or removed).