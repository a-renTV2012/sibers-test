class presenter {
    static currentOrderField = 'login';
    static currentOrderReversed = false;
    static currentPage = 1;
    static recordsPerPage = 5;

    static async displayUsers(orderField = null) {
        if (orderField != null)
            this.currentOrderField = orderField;
        else
            orderField = this.currentOrderField;

        await $.ajax({
            type: 'POST',
            url: 'php/api/retrieveAll.php',
            data: {
                orderField: orderField,
                startId: (presenter.currentPage - 1) * presenter.recordsPerPage + 1,
                endId: presenter.currentPage * presenter.recordsPerPage,
                reversedOrder: presenter.currentOrderReversed,
                pageKey: $('.page-key').val()
            }
        }).done(function(response) {
            try {
                response = JSON.parse(response);

                $('.record').remove();

                $.each(response.reverse(), function(index, value) {
                    $('.bar').after('<div class="row justify-content-between record" id="record_'+ value[0] +'"><div class="col-6 left"><p class="record-user">' + value[1] + ' - ' + value[4] + ' ' + value[3] + '&nbsp;' + '</p><div class="buttons-wrap"><div class="record-button record-edit button" id="edit_'+ value[0] +'" onclick="presenter.renderUpdateForm(' + value[0] + ');"></div>&nbsp;<div class="record-button record-remove button" id="remove_'+ value[0] +'" onclick="presenter.renderDeleteForm(' + value[0] + ');"></div></div></div><div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4 right"><p class="record-dob">DOB: '+ value[6] +'</p><div class="wrap"><p class="record-gender">Gender: '+ value[5] +'</p></div></div></div>');  
                });
            } catch (error) {
                presenter.renderResponse(response);
                console.log(error);
            }
        });

        return new Promise(resolve => {resolve(1);});
    }

    static displayPageController() {
        $('.record').last().after(`
            <div class="row justify-content-center page-controller">
                <button class="col-2 page-controller-button button" onclick="presenter.displayPrevPage();">Prev</button>
                <span class="col-1 page-controller-page">` + presenter.currentPage + `</span>
                <button class="col-2 page-controller-button button" onclick="presenter.displayNextPage();">Next</button>
            </div>
        `);
    }

    static async displayPrevPage() {
        if (this.currentPage > 1) {
            this.currentPage -= 1;
            await this.displayUsers();
            $('.page-controller').remove();
            this.displayPageController();
        }
    }

    static async displayNextPage() {
        if ($('div.record').length == this.recordsPerPage) {
            this.currentPage += 1;
            await this.displayUsers();
            $('.page-controller').remove();
            this.displayPageController();
        }   
    }

    static renderCreateForm() {
        $('body').append(`
            <div class="overlay">
                <div class="overlay-close button" onclick="$('.overlay').remove();">&times;</div>

                <form class="overlay-form container" onsubmit="updater.createUser();">
                    <label for="overlay-form-login">Login</label><br>
                    <input class="overlay-form-login scalable-input" type="text" placeholder="Login" required><br>

                    <label for="overlay-form-password">Password</label><br>
                    <input class="overlay-form-password scalable-input" type="password" placeholder="Password" required><br>

                    <label for="overlay-form-name">Name</label><br>
                    <input class="overlay-form-name scalable-input" type="text" placeholder="Name" required><br>

                    <label for="overlay-form-surname">Surname</label><br>
                    <input class="overlay-form-surname scalable-input" type="text" placeholder="Surname" required><br>

                    <label for="overlay-form-gender">Gender</label><br>
                    <input class="overlay-form-gender scalable-input" type="text" placeholder="Gender" required><br>

                    <label for="overlay-form-dob">Date of birth</label><br>
                    <input class="overlay-form-dob scalable-input" type="date" placeholder="Date of birth" required><br>

                    <label for="overlay-form-is-admin">Is admin?</label>
                    <input class="overlay-form-is-admin" type="checkbox"><br>

                    <button type="submit">Submit</button>
                </form>
            </div>
        `);
    }

    static renderUpdateForm(id) {
        this.renderCreateForm();
        $('.overlay-form').attr('onsubmit', 'updater.updateUser();');
        $('.overlay-form').append('<input class="overlay-form-id" type="text" value="' + id + '" hidden>');

        $.ajax({
            type: 'POST',
            url: 'php/api/retrieve.php',
            data: {id: $('.overlay-form-id').val(), pageKey: $('.page-key').val()}
        }).done(function(response) {
            try {
                response = JSON.parse(response);

                $('.overlay-form-login').val(response[1]);
                $('.overlay-form-password').val(response[2]);
                $('.overlay-form-name').val(response[3]);
                $('.overlay-form-surname').val(response[4]);
                $('.overlay-form-gender').val(response[5]);
                $('.overlay-form-dob').val(response[6]);

                if (response[7] == 1)
                    $('.overlay-form-is-admin').attr('checked', 'checked');
            } catch (error) {
                presenter.renderResponse(response);
                console.log(error);
            }
            
        });
    }

    static renderDeleteForm(id) {
        $('body').append(`
            <div class="overlay">
                <div class="overlay-close button" onclick="$('.overlay').remove();">&times;</div>

                <form class="overlay-form container" onsubmit="updater.deleteUser();">
                    <input class="overlay-form-id" type="text" value="` + id + `" hidden>
                    <p>Are you sure you want to delete that user?</p>

                    <button type="submit">Yes</button>
                    <button onclick="$('.overlay').remove();">No</button>
                </form>
            </div>
        `);
    }

    static renderResponse(response) {
        if (response.toLowerCase().includes('success')) {
            $('.overlay').remove();
            this.displayUsers();
        }

        alert(response);
    }
}

class updater {
    static createUser() {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'php/api/create.php',
            data: {
                login: $('.overlay-form-login').val(),
                passhash: $('.overlay-form-password').val(),
                name: $('.overlay-form-name').val(),
                surname: $('.overlay-form-surname').val(),
                gender: $('.overlay-form-gender').val(),
                dob: $('.overlay-form-dob').val(),
                isAdmin: $('.overlay-form-is-admin').prop('checked'),
                pageKey: $('.page-key').val()
            }
        }).done(function(response) {
            presenter.renderResponse(response);
        });
    }

    static updateUser() {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'php/api/update.php',
            data: {
                id: $('.overlay-form-id').val(),
                login: $('.overlay-form-login').val(),
                passhash: $('.overlay-form-password').val(),
                name: $('.overlay-form-name').val(),
                surname: $('.overlay-form-surname').val(),
                gender: $('.overlay-form-gender').val(),
                dob: $('.overlay-form-dob').val(),
                isAdmin: $('.overlay-form-is-admin').prop('checked'),
                pageKey: $('.page-key').val()
            }
        }).done(function(response) {
            presenter.renderResponse(response);
        });
    }

    static deleteUser() {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'php/api/delete.php',
            data: {id: $('.overlay-form-id').val(), pageKey: $('.page-key').val()}
        }).done(function(response) {
            presenter.renderResponse(response);
        });
    }
}

async function init() {
    await presenter.displayUsers();
    presenter.displayPageController();
}

$(document).ready(function () {
    init();
});