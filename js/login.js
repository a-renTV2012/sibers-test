function authenticate(sessionKey) {
    event.preventDefault();

    $.ajax({
        type: 'POST',
        url: 'php/api/authenticate.php',
        data: {
            login: $('.login-form-login').val(),
            passhash: $('.login-form-password').val(),
            sessionKey: sessionKey,
        }
    }).done(function(response) {
        if (response == 1)
            window.location.reload();
        else
            alert(response);
    });
}