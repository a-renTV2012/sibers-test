<?php 
    session_start();

    require_once($_SERVER['DOCUMENT_ROOT'] . '/php/settings.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/php/usersManager.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authenticator.php');

    $conn = new mysqli($hostname = $settings['mysql']['ip'], $username =  $settings['mysql']['user'], $password =  $settings['mysql']['password'], $database = $settings['mysql']['db'],  $settings['mysql']['port']);

    usersManager::setConnection($conn);
?>

    <!DOCTYPE html>

    <html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta charset="UTF-8">

            <title>Sibers test</title>

<?php if (!authenticator::validateSession(session_id())) {?>
            <link rel="stylesheet" href="css/login.css" type="text/css">
        </head>

        <body>
            <form class="login-form" method="POST" onsubmit="authenticate('<?php echo(session_id()) ?>');">
                <input class="login-form-login" type="text" placeholder="Login" required><br>
                <input class="login-form-password" type="password" placeholder="Password" required><br>

                <button class="login-form-button" type="submit">Login</button>
            </form>

            <script scr="js/jquery-3.3.1.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
            <script src="js/login.js"></script>
        </body>
    </html>

<?php } else { ?>

        <link rel="stylesheet" href="css/bootstrap-grid.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>

    <body>
        <div class="container">
            <div class="row bar">
                <div class="bar-sort col-9">
                    <span class="bar-sort-span">Sort by:</span>
                    <button class="bar-button bar-login" onclick="presenter.displayUsers('login');">Login</button>
                    <button class="bar-button bar-name" onclick="presenter.displayUsers('name');">Name</button>
                    <button class="bar-button bar-surname" onclick="presenter.displayUsers('surname');">Surname</button>
                    <button class="bar-button bar-gender" onclick="presenter.displayUsers('gender');">Gender</button>
                    <button class="bar-button bar-dob" onclick="presenter.displayUsers('dob');">Date of birth</button>
                </div>

                <div class="bar-add col-3">
                    <button class="bar-button" onclick="presenter.renderCreateForm();">Create user</button>
                </div>
            </div>    
        </div>


        <script scr="js/jquery-3.3.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-3.3.1.min.js"><\/script>')</script>
        <script src="js/index.js"></script>
        
        <input class="page-key" type="text" value="<?php
            $pageKey = bin2hex(random_bytes(16)); // That value can confirm that an admin is trying to access the API via interface, not direct requests
            authenticator::setPageKey($pageKey, session_id());
            echo($pageKey);
        ?>" hidden>
    </body>
</html>

<?php } ?>