<?php

class authenticator {
    // Is it the same user that before? Can he access the interface?
    public static function validateSession($sessionKey) {
        $user = usersManager::retrieveByField('session_key', $sessionKey)->fetch_all()[0];

        if (!empty($user)) {
            if ($user[9] + 86400 > time())
                return true;
            else
                return false;
        }
    }

    // Is it the same page key that we sent to client?
    public static function validatePageKey($pageKey) {
        if (!empty(usersManager::retrieveByField('page_key', $pageKey)->fetch_all()[0]))
            return true;
        else
            return false;
    }

    // Saving session parameters to db after authentication
    public static function setSession($login, $passhash, $sessionKey) {
        $user = usersManager::retrieveByField('login', $login)->fetch_all()[0];

        if (!empty($user)) {
            if ($user[2] == $passhash) {
                if ($user[7] == 1)
                    return usersManager::updateUser($user[0], $user[1], $user[2], $user[3], $user[4], $user[5], $user[6], $user[7], $sessionKey, time(), $user[10]);
                else
                    return 'You don\'t have necessary rights.';
            }  
        }

        return 'Login or password are incorrect.';
    }

    // Saving page key to db when sending the page
    public static function setPageKey($pageKey, $sessionKey) {
        $user = usersManager::retrieveByField('session_key', $sessionKey)->fetch_all()[0];

        usersManager::updateUser($user[0], $user[1], $user[2], $user[3], $user[4], $user[5], $user[6], $user[7], $user[8], $user[9], $pageKey);
    }
}

?>