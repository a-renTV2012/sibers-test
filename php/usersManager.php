<?php

class usersManager {
    private static $connection;

    // The connection MUST be set before trying to make any query. This is done via call in api/<handler>.php
    public static function setConnection(mysqli $connection) {
        static::$connection = $connection;
    }

    // Escaping injections and returning nulls as strings (so they can be concatenated with the rest of the query)
    public static function escapeForSql($value) {
        if (is_string($value)) 
            $value = '"' . static::$connection->real_escape_string($value) . '"';
        elseif (is_null($value))
            $value = 'NULL';
        elseif ($value === true)
            $value = 'true';
        elseif ($value === false)
            $value = 'false';
        
        return $value;
    }

    // Checking if there is the same login already or not
    public static function checkLoginAvailability($login) {
        if (!empty(static::$connection->query('SELECT * FROM users WHERE login = ' . static::escapeForSql($login) . ';')->fetch_all()[0]))
            return false;
        else
            return true;
    }

    // Updating an existent user
    public static function updateUser($id, $login, $passhash, $name, $surname, $gender, $dob, $isAdmin, $sessionKey = null, $sessionTimestamp = null, $pageKey = null) {
        $user = static::retrieveUser($id);
        
        if (static::checkLoginAvailability($login) || $user[1] == $login) {
            $fieldsList = ['login', 'passhash', 'name', 'surname', 'gender', 'dob', 'is_admin', 'session_key', 'session_timestamp', 'page_key'];
            $args = func_get_args();
            $setStr = '';

            for ($i = 1; $i < 11; $i++) {
                if ($args[$i] == null)
                    $args[$i] = $user[$i];

                $setStr = $setStr . $fieldsList[$i - 1] . ' = ' . static::escapeForSql($args[$i]) . ', ';
            }

            $setStr = rtrim($setStr, ' ,');

            return static::$connection->query('UPDATE users SET ' . $setStr . ' WHERE id = ' . $id . ';');
        } else {
            return 'This login is already present in the database.';
        }
    }

    // Creating a new user
    public static function createUser($login, $passhash, $name, $surname, $gender, $dob, $isAdmin) {
        if (static::checkLoginAvailability($login)) {
            $id = (int)static::$connection->query('SELECT MAX(id) FROM users;')->fetch_all()[0][0] + 1;
            $args = func_get_args();
            $valuesStr = $id . ', ';

            for ($i = 0; $i < 7; $i++) {
                $valuesStr = $valuesStr . static::escapeForSql($args[$i]) . ', ';
            }

            $valuesStr = rtrim($valuesStr, ' ,');

            return static::$connection->query('INSERT INTO users (id, login, passhash, name, surname, gender, dob, is_admin) VALUES (' . $valuesStr . ');');
        } else {
            return 'This login is already present in the database.';
        }
    }

    // Deleting a user record
    public static function deleteUser($id) {
        return static::$connection->query('DELETE FROM users WHERE id = ' . static::escapeForSql($id) . ';');
    }

    // Retrieving a user by ID
    public static function retrieveUser($id) {
        return static::$connection->query('SELECT * FROM users WHERE id = ' . static::escapeForSql($id) . ';')->fetch_all()[0];
    }

    // Retrieving user(s) by the value of any field
    public static function retrieveByField($field, $value) {
        return static::$connection->query('SELECT * FROM users WHERE ' . $field . ' = ' . static::escapeForSql($value) . ';');
    }

    // Retrieving all users
    public static function retrieveAll($orderField, $startId, $endId, $reversedOrder) {
        $orderByStr = 'ORDER BY ' . $orderField;

        if ($reversedOrder == 'true')
            $orderByStr = $orderByStr . ' DESC';

        static::$connection->query('SET @row_number = 0;');
        return static::$connection->query('SELECT id, login, passhash, name, surname, gender, dob FROM (SELECT id, login, passhash, name, surname, gender, dob, (@row_number:=@row_number + 1) AS rownum FROM users ' . $orderByStr . ') t WHERE rownum >= ' . static::escapeForSql($startId) . ' AND rownum <= ' . static::escapeForSql($endId) . ';')->fetch_all();
    }
}

?>