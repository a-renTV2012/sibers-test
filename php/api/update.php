<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/php/settings.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/usersManager.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authenticator.php');

$conn = new mysqli($hostname = $settings['mysql']['ip'], $username =  $settings['mysql']['user'], $password =  $settings['mysql']['password'], $database = $settings['mysql']['db'],  $settings['mysql']['port']);

usersManager::setConnection($conn);

$id = $_POST['id'];
$login = $_POST['login'];
$passhash = $_POST['passhash'];
$name = $_POST['name'];
$surname = $_POST['surname'];
$gender = $_POST['gender'];
$dob = $_POST['dob'];
$isAdmin = $_POST['isAdmin'];
$pageKey = $_POST['pageKey'];

if (authenticator::validatePageKey($pageKey)) {
    $dataCorrect = true;

    foreach ($_POST as $value) {
        if ($value == null) {
            $dataCorrect = false;
            break;
        }

        if ($value != $id) {
            if ($value != $dob) {
                if ($value != $login && $value != $pageKey) {
                    if (preg_match('/[^A-z]/', $value)) {
                        $dataCorrect = false;
                        break;
                    }
                } else {
                    if (preg_match('/[^A-z_.0-9]/', $value)) {
                        $dataCorrect = false;
                        break;
                    }
                }
            } else {
                if (!preg_match('/([\d]{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])/', $value)) {
                    $dataCorrect = false;
                    break;
                }
            }

            if (strlen($value) > 32) {
                $dataCorrect = false;
                break;
            }
            
        }
    }

    if ($isAdmin == 'true') 
        $isAdmin = true;
    else
        $isAdmin = false;

    if (is_string($id) && (int)$id != 0) { // Forms send numbers as strings so we must cast (int). We also need string check, because if i.e. someone pass a non-empty array as "id", (int) will return 1. And if one passes a string, it will be converted to 0 (which is not used in indexes).
        if ($dataCorrect) {
            $returned = usersManager::updateUser((int)$id, $login, $passhash, $name, $surname, $gender, $dob, $isAdmin);

            if ($returned === 'This login is already present in the database.')
                echo($returned);
            elseif ($returned)
                echo('Successfully updated a user record!');
            else
                echo('Oops. Something went wrong!');
        } else {
            echo('Please provide clear input: no special characters or numbers must be present in strings and DOB should be formatted like YYYY-MM-DD. All fields must not contain more than 32 characters.');
        }
    } else {
        echo('Record id must be int. Make sure you\'re using a proper interface.');
    }
} else {
    echo('Server received an incorrect page identifier. Are you using something other than the web interface?');
}
?>