<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/php/settings.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/usersManager.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authenticator.php');

$conn = new mysqli($hostname = $settings['mysql']['ip'], $username =  $settings['mysql']['user'], $password =  $settings['mysql']['password'], $database = $settings['mysql']['db'],  $settings['mysql']['port']);

usersManager::setConnection($conn);

$orderField = $_POST['orderField'];
$startId = $_POST['startId'];
$endId = $_POST['endId'];
$reversedOrder = $_POST['reversedOrder'];
$pageKey = $_POST['pageKey'];

if (authenticator::validatePageKey($pageKey)) {
    if ($orderField != null && is_string($orderField) && $reversedOrder != null && is_string($reversedOrder) && $startId != null && is_string($startId) && (int)$startId != 0 && $endId != null && is_string($endId) && (int)$endId != 0) {
        $reversedOrder = $conn->real_escape_string($reversedOrder);

        echo(json_encode(usersManager::retrieveAll($orderField, (int)$startId, (int)$endId, $reversedOrder)));
    } else {
        echo('Some values are missing or have an incorrect type. Make sure you\'re using a proper interface.');
    }
} else {
    echo('Server received an incorrect page identifier. Are you using something other than the web interface?');
}
?>