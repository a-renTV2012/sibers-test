<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/php/settings.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/usersManager.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authenticator.php');

$conn = new mysqli($hostname = $settings['mysql']['ip'], $username =  $settings['mysql']['user'], $password =  $settings['mysql']['password'], $database = $settings['mysql']['db'],  $settings['mysql']['port']);

usersManager::setConnection($conn);

$login = $_POST['login'];
$passhash = $_POST['passhash'];
$sessionKey = $_POST['sessionKey'];

if (is_string($login) && is_string($passhash) && is_string($sessionKey)) {
    echo(authenticator::setSession($login, $passhash, $sessionKey));
} else {
    echo('Some data is corrupted or missing. Please try to login again.');
}

?>