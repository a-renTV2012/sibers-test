<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/php/settings.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/usersManager.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authenticator.php');

$conn = new mysqli($hostname = $settings['mysql']['ip'], $username =  $settings['mysql']['user'], $password =  $settings['mysql']['password'], $database = $settings['mysql']['db'],  $settings['mysql']['port']);

usersManager::setConnection($conn);

$login = $_POST['login'];
$passhash = $_POST['passhash'];
$name = $_POST['name'];
$surname = $_POST['surname'];
$gender = $_POST['gender'];
$dob = $_POST['dob'];
$isAdmin = $_POST['isAdmin'];
$pageKey = $_POST['pageKey'];

if (authenticator::validatePageKey($pageKey)) {
    $dataCorrect = true;

    foreach ($_POST as $value) {
        if ($value == null) {
            $dataCorrect = false;
            break;
        }

        if ($value != $dob) {
            if ($value != $login && $value != $pageKey) {
                if (preg_match('/[^A-z]/', $value)) {
                    $dataCorrect = false;
                    break;
                }
            } else {
                if (preg_match('/[^A-z_.0-9]/', $value)) {
                    $dataCorrect = false;
                    break;
                }
            }
        } else {
            if (!preg_match('/([\d]{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])/', $value)) {
                $dataCorrect = false;
                break;
            }
        }

        if (strlen($value) > 32) {
            $dataCorrect = false;
            break;
        }
    }

    if ($isAdmin == 'on') 
        $isAdmin = true;
    else
        $isAdmin = false;

    if ($dataCorrect) {
        $returned = usersManager::createUser($login, $passhash, $name, $surname, $gender, $dob, $isAdmin);

        if ($returned === 'This login is already present in the database.')
            echo($returned);
        elseif ($returned)
            echo('Successfully added a user record!');
        else    
            echo('Oops. Something went wrong!');
    } else {
        echo('Please provide clear input: no special characters or numbers must be present in strings and DOB should be formatted like YYYY-MM-DD. All fields must not contain more than 32 characters.');
    }
} else {
    echo('Server received an incorrect page identifier. Are you using something other than the web interface?');
}
?>