<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/php/settings.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/usersManager.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/php/authenticator.php');

$conn = new mysqli($hostname = $settings['mysql']['ip'], $username =  $settings['mysql']['user'], $password =  $settings['mysql']['password'], $database = $settings['mysql']['db'],  $settings['mysql']['port']);

usersManager::setConnection($conn);

$id = $_POST['id'];
$pageKey = $_POST['pageKey'];

if (authenticator::validatePageKey($pageKey)) {
    if (is_string($id) && (int)$id != 0)
        echo(json_encode(usersManager::retrieveUser((int)$id)));
    else
        echo('Record id must be int. Make sure you\'re using a proper interface.');
} else {
    echo('Server received an incorrect page identifier. Are you using something other than the web interface?');
}

?>