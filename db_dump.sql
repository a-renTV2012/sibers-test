-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 20 2021 г., 17:10
-- Версия сервера: 5.7.20-log
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sibers_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` tinytext NOT NULL,
  `passhash` tinytext NOT NULL,
  `name` tinytext,
  `surname` tinytext,
  `gender` tinytext,
  `dob` date DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `session_key` tinytext,
  `session_timestamp` int(11) DEFAULT NULL,
  `page_key` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `passhash`, `name`, `surname`, `gender`, `dob`, `is_admin`, `session_key`, `session_timestamp`, `page_key`) VALUES
(1, 'Maxer', 'qwer', 'Maxim', 'Petrov', 'M', '2002-06-07', 0, NULL, NULL, NULL),
(2, 'max', 'qwer', 'Maxim', 'Ivanov', 'M', '2012-03-05', 0, NULL, NULL, NULL),
(3, 'food', 'qwer', 'Anton', 'Bulochkin', 'M', '2010-10-17', 0, NULL, NULL, NULL),
(4, 'Nata_JR', 'qwer', 'Sergey', 'Spy', 'M', '2005-09-09', 0, NULL, NULL, NULL),
(5, 'Simple_nik', 'asdf', 'Nikita', 'Trubov', 'M', '2004-03-05', 0, NULL, NULL, NULL),
(6, 'Arcadiy', 'qwer', 'Arcadiy', 'Lucky', 'M', '2001-07-07', 0, NULL, NULL, NULL),
(7, 'Support_Lena', 'asdf', 'Lena', 'Borisovna', 'F', '2021-01-01', 0, NULL, NULL, NULL),
(8, 'Support_Alexey', 'asdf', 'Alexey', 'Frolov', 'M', '2000-12-20', 0, NULL, NULL, NULL),
(9, 'Andreyno', 'Secret', 'Andrey', 'Anchugin', 'M', '2002-02-15', 1, 'if0fs13h1986sfr0rsqd4hh29lo01629', 1629467703, 'f5138074ced024bc7f73a361c32499bb'),
(10, 'Computer_10057', 'asdf', 'name', 'No', 'C', '2020-05-05', 0, NULL, NULL, NULL),
(11, 'Computer_10058', 'asdf', 'name', 'No', 'C', '2020-05-05', 0, NULL, NULL, NULL),
(12, 'Computer_10059', 'asdf', 'name', 'No', 'C', '2020-05-05', 0, NULL, NULL, NULL),
(13, 'Kass', 'zxcv', 'Mark', 'Vorobyev', 'M', '2002-02-02', 0, NULL, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
